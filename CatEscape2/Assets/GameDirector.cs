﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public int Co = 0;
    public GameObject LifeGauge;
    public GameObject arrowCount = null;

    // Start is called before the first frame update
    void Start()
    {
        this.LifeGauge = GameObject.Find("LifeGauge");
    }

    public void DecreaseHp()
    {
        this.LifeGauge.GetComponent<Image>().fillAmount -= 0.1f;
    }

    public void DecreaseCo()
    {
        Co++;
    }

    void Update()
    {
        Text arrowText = this.arrowCount.GetComponent<Text>();
        arrowText.text = "スコア：" + Co;

        if (this.LifeGauge.GetComponent<Image>().fillAmount == 0)
            SceneManager.LoadScene("GameOverScene");



    }
}
