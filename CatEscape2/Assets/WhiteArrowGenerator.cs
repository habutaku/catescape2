﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteArrowGenerator : MonoBehaviour
{
    public GameObject WhiteArrowPrefab;
    float span = 0.5f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta>this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(WhiteArrowPrefab) as GameObject;
            int px = Random.Range(-4, 3);
            go.transform.position = new Vector3(7, px, 0);
        }
    }
}
