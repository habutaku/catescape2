﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteArrowController : MonoBehaviour
{

    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);
        if (transform.position.x < -12.0f)
        {
            Destroy(gameObject);
        }
        Vector2 b1 = transform.position;
        Vector2 b2 = this.player.transform.position;
        Vector2 bth = b1 - b2;
        float t = bth.magnitude;
        float h1 = 0.5f;
        float h2 = 1.0f;

        if (t < h1 + h2)
        {

            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();

            Destroy(gameObject);
        }
    }
}
